import sqlite3

# Connect to database file
conn = sqlite3.connect('database.db')
cur = conn.cursor()

# Execute sql script to create tables
with open('database.sql', 'r') as f:
    sql_script = f.read()
cur.executescript(sql_script)

# Insert some data into tables
cur.execute("INSERT INTO cars (model, year, color, plate) VALUES ('Toyota Corolla', 2010, 'white', 'ABC-123')")
cur.execute("INSERT INTO cars (model, year, color, plate) VALUES ('Honda Civic', 2012, 'black', 'XYZ-456')")
cur.execute("INSERT INTO clients (name, phone, email) VALUES ('John Smith', '+1 234 567 8901', 'john.smith@example.com')")
cur.execute("INSERT INTO clients (name, phone, email) VALUES ('Mary Jones', '+1 987 654 3210', 'mary.jones@example.com')")
cur.execute("INSERT INTO services (name, price) VALUES ('Oil change', 50.0)")
cur.execute("INSERT INTO services (name, price) VALUES ('Tire rotation', 30.0)")
cur.execute("INSERT INTO orders (car_id, client_id, service_id, date, status) VALUES (1, 1, 1, '2023-09-10', 'pending')")
cur.execute("INSERT INTO orders (car_id, client_id, service_id, date, status) VALUES (2, 2, 2, '2023-09-11', 'in progress')")

# Commit changes to database
conn.commit()

# Query some data from tables
cur.execute("SELECT * FROM cars")
cars = cur.fetchall()
print("Cars:")
for car in cars:
    print(car)

cur.execute("SELECT * FROM clients")
clients = cur.fetchall()
print("Clients:")
for client in clients:
    print(client)

cur.execute("SELECT * FROM services")
services = cur.fetchall()
print("Services:")
for service in services:
    print(service)

cur.execute("SELECT * FROM orders")
orders = cur.fetchall()
print("Orders:")
for order in orders:
    print(order)

# Close database connection
conn.close()
