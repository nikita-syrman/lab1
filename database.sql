-- database.sql
-- Create database file
PRAGMA foreign_keys = ON;
BEGIN TRANSACTION;

-- Create table cars
CREATE TABLE IF NOT EXISTS cars (
    id INTEGER PRIMARY KEY,
    model TEXT NOT NULL,
    year INTEGER NOT NULL,
    color TEXT NOT NULL,
    plate TEXT NOT NULL UNIQUE
);

-- Create table clients
CREATE TABLE IF NOT EXISTS clients (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    phone TEXT NOT NULL UNIQUE,
    email TEXT NOT NULL UNIQUE
);

-- Create table services
CREATE TABLE IF NOT EXISTS services (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    price REAL NOT NULL
);

-- Create table orders
CREATE TABLE IF NOT EXISTS orders (
    id INTEGER PRIMARY KEY,
    car_id INTEGER NOT NULL REFERENCES cars(id) ON DELETE CASCADE ON UPDATE CASCADE,
    client_id INTEGER NOT NULL REFERENCES clients(id) ON DELETE CASCADE ON UPDATE CASCADE,
    service_id INTEGER NOT NULL REFERENCES services(id) ON DELETE CASCADE ON UPDATE CASCADE,
    date TEXT NOT NULL,
    status TEXT NOT NULL CHECK (status IN ('pending', 'in progress', 'done', 'canceled'))
);

COMMIT;
